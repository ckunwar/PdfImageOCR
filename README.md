# PdfImageOCR
**Requirement:**<br/>
Pytorch<br/>
MMOCR<br/>
MMDetection<br/>
CUDA<br/>
tesseract-ocr<br/>
Python 3.6<br/>
**Objective:**<br/>
The objective is to recognize character from pdf images using deep learning based techniques.
